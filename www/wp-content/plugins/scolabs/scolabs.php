<?php
/**
 * Plugin Name: Scolabs
 * Description: Plugin to manage planning
 * Version: 0.1
 * Author: Scolabs
 * License: MIT
 * Requires PHP: 7.0
 */

class Scolabs {

	/**
	 * Scolabs constructor.
	 */
	public function __construct() {
		if ( version_compare( PHP_VERSION, '7.0.0', '<' ) ) {
			add_filter( 'wp_title', [ $this, 'scolabs_modify_page_title' ], 20 );
		}
		add_action( 'admin_menu', [ $this, 'scolabs_setup_menu' ] );
		add_action( 'admin_action_post', [ $this, 'admin_action' ] );
		register_activation_hook( __FILE__, [ $this, 'scolabs_init_db' ] );
		register_deactivation_hook( __FILE__, [ $this, 'scolabs_destroy_db' ] );
		add_shortcode( 'planning', [ $this, 'scolabs_init_shortcode' ] );
	}

	/**
	 * @param $title
	 *
	 * @return string
	 */
	public function scolabs_modify_page_title( $title ) {
		return $title . ' | Scolabs';
	}

	/**
	 * Initialise menu nav
	 */
	public function scolabs_setup_menu() {
		add_menu_page(
			'Import des classes', // page <title>Title</title>
			'Scolabs', // menu link text
			'manage_options', // capability to access the page
			'scolabs-menu', // page URL slug
			[ $this, 'scolabs_page_content' ], // callback function /w content
			'dashicons-star-half', // menu icon
			30 // priority
		);

		add_submenu_page(
			'scolabs-menu',
			'Import des classes',
			'Import des classes',
			'manage_options',
			'import-classrooms',
			[
				$this,
				'scolabs_import_classrooms_front'
			]
		);

		add_submenu_page(
			'scolabs-menu',
			'Import des planning',
			'Import des planning',
			'manage_options',
			'import-planning',
			[
				$this,
				'scolabs_import_planning_front'
			]
		);
	}

	/**
	 * Front of import classroom page
	 */
	public function scolabs_import_classrooms_front() {
		$this->scolabs_handle_post_classrooms();
		$this->display_flash_notices();
		?>
        <h1>Import des classes!</h1>
        <!-- Form to handle the upload - The enctype value here is very important -->
        <form method="post" enctype="multipart/form-data">
            <input type='file' id='classrooms' name='classrooms' accept=".csv"/>
			<?php submit_button( 'Importer' ) ?>
        </form>
		<?php
	}

	/**
	 * Front of import planning page
	 */
	public function scolabs_import_planning_front() {
		$this->scolabs_handle_post_planning();
		$this->display_flash_notices();
		?>
        <h1>Import des planning !</h1>
        <!-- Form to handle the upload - The enctype value here is very important -->
        <form method="post" enctype="multipart/form-data">
            <input type='file' id='planning' name='planning' accept=".csv"/>
			<?php submit_button( 'Importer' ) ?>
        </form>
		<?php
	}

	/**
	 * Main page scolabs
	 */
	public function scolabs_page_content() {
		echo '<p>Le plugin permet d\'importer les plannings et les classes sur votre site Wordpress</p>';
	}

	/**
	 * Init table
	 */
	public function scolabs_init_db() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		global $wpdb;
		$tableName = $wpdb->prefix . 'scolabs_classrooms';
		if ( $wpdb->get_var( 'SHOW TABLES LIKE ' . $tableName ) != $tableName ) {
			$sql = 'CREATE TABLE ' . $tableName . '(id int auto_increment,
                    name varchar(255) not null,
                    constraint scolabs_pk
                        primary key (id)
                    );';
			dbDelta( $sql );

		}
		$tableName = $wpdb->prefix . 'scolabs_planning';
		if ( $wpdb->get_var( 'SHOW TABLES LIKE ' . $tableName ) != $tableName ) {
			$sql = 'CREATE TABLE ' . $tableName . '
                (
                    id int auto_increment,
                    subject varchar(255) not null,
                    start_hour char(5) not null,
                    id_classroom int not null,
                    constraint scolabs_planning_pk
                        primary key (id),
                    constraint scolabs_planning_scolabs_classrooms_id_fk
                        foreign key (id_classroom) references ' . $wpdb->prefix . 'scolabs_classrooms (id)
                            on update cascade on delete cascade
                );';
			dbDelta( $sql );
		}
	}

	/**
	 * Delete table when disable plugin
	 */
	public function scolabs_destroy_db() {
		global $wpdb;
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		$sql = 'DROP TABLE ' . $wpdb->prefix . 'scolabs_planning;';
		$wpdb->query( $sql );
		$sql = 'DROP TABLE ' . $wpdb->prefix . 'scolabs_classrooms;';
		$wpdb->query( $sql );
	}

	/**
	 * Upload file and insert in db
	 */
	public function scolabs_handle_post_classrooms() {
		global $wpdb;
		// First check if the file appears on the _FILES array
		if ( isset( $_FILES['classrooms'] ) ) {
			if ( ! function_exists( 'wp_handle_upload' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			$csv = $_FILES['classrooms'];

			$upload_overrides = array( 'test_form' => false );
			$moveFile         = wp_handle_upload( $csv, $upload_overrides );
			if ( $moveFile ) {
				$wpdb->query( 'SET FOREIGN_KEY_CHECKS = 0; ' );
				$wpdb->query( 'TRUNCATE ' . $wpdb->prefix . 'scolabs_planning' );
				$wpdb->query( 'TRUNCATE ' . $wpdb->prefix . 'scolabs_classrooms' );
				$wpdb->query( 'SET FOREIGN_KEY_CHECKS = 1; ' );
				$contentOfFile = file_get_contents( $moveFile['file'] );
				$rows          = explode( ';', $contentOfFile );
				$tableName     = $wpdb->prefix . 'scolabs_classrooms';
				foreach ( $rows as $row ) {
					if ( ! empty( trim( $row ) ) ) {
						$wpdb->insert( $tableName, [ 'name' => trim( $row ) ], [ '%s' ] );
					}
				}
				unlink( $moveFile['file'] );
				$notices = get_option( "my_flash_notices", array() );
				array_push( $notices, [
					'notice'      => 'Les classes ont bien été importées',
					'type'        => 'success',
					'dismissible' => true
				] );
				update_option( "my_flash_notices", $notices );
			} else {
				unlink( $moveFile['file'] );
				$notices = get_option( "my_flash_notices", array() );
				array_push( $notices, [
					'notice'      => 'Les classes n\'ont pas pu être importées',
					'type'        => 'danger',
					'dismissible' => true
				] );
				update_option( "my_flash_notices", $notices );
			}
		}
	}

	public function scolabs_handle_post_planning() {
		global $wpdb;
		// First check if the file appears on the _FILES array
		if ( isset( $_FILES['planning'] ) ) {
			if ( ! function_exists( 'wp_handle_upload' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			$csv = $_FILES['planning'];

			$upload_overrides = array( 'test_form' => false );
			$moveFile         = wp_handle_upload( $csv, $upload_overrides );
			if ( $moveFile ) {
				$wpdb->query( 'TRUNCATE ' . $wpdb->prefix . 'scolabs_planning' );
				$file          = fopen( $moveFile['file'], 'r' );
				$contentOfFile = fgetcsv( $file );
				$tableName     = $wpdb->prefix . 'scolabs_planning';
				$days          = [ 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ];
				$classroom_id  = null;

				$classrooms  = explode( ';', $contentOfFile[0] );
				$i           = - 1;
				$daysOfWeeks = [];
				while ( $data = fgetcsv( $file ) ) {
					$dataExploded = explode( ';', $data[0] );
					if ( in_array( $dataExploded[0], $days ) ) {
						$daysOfWeeks = $data;
						$i ++;
					} else {
						$classroom_id = intval( $wpdb->get_row( 'SELECT id FROM ' . $wpdb->prefix . 'scolabs_classrooms WHERE name = "' . $classrooms[ $i ] . '"' )->id );
						$hour         = $dataExploded[0];
						unset( $dataExploded[0] );
						$dataExploded = array_values( $dataExploded );
						foreach ( $dataExploded as $item ) {
							if ( ! empty( trim( $item ) ) ) {
								$wpdb->insert( $tableName, [
									'subject'      => $item,
									'start_hour'   => ( strlen( $hour ) === 4 ) ? '0' . $hour : $hour,
									'id_classroom' => $classroom_id
								], [ '%s', '%s', '%d' ] );
							}
						}
					}
				}
				unlink( $moveFile['file'] );
				$notices = get_option( "my_flash_notices", array() );
				array_push( $notices, [
					'notice'      => 'L\'emploi du temps a bien été mis à jour',
					'type'        => 'success',
					'dismissible' => true
				] );
				update_option( "my_flash_notices", $notices );
			} else {
				unlink( $moveFile['file'] );
				$notices = get_option( "my_flash_notices", array() );
				array_push( $notices, [
					'notice'      => 'L\'emploi du temps n\'a pas pu être mis à jour',
					'type'        => 'danger',
					'dismissible' => true
				] );
				update_option( "my_flash_notices", $notices );
			}
		}
	}

	public function display_flash_notices() {
		$notices = get_option( "my_flash_notices", array() );

		// Iterate through our notices to be displayed and print them.
		foreach ( $notices as $notice ) {
			printf( '<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
				$notice['type'],
				$notice['dismissible'],
				$notice['notice']
			);
		}

		// Now we reset our options to prevent notices being displayed forever.
		if ( ! empty( $notices ) ) {
			delete_option( "my_flash_notices" );
		}
	}

	/**
	 * Shortcode Settings
	 */
	public function scolabs_init_shortcode( $atts ) {
		global $wpdb;
		$rows = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'scolabs_planning p LEFT JOIN ' . $wpdb->prefix . 'scolabs_classrooms c ON  c.id = p.id_classroom WHeRE c.name = "' . $atts['classroom'] . '"' );

		$planning = [];

		$i = 0;
		foreach ( $rows as $row ) {
			$planning[ $row->start_hour ][ $i ] = $row->subject;
			$i ++;
		}

		$return =
			'<table>
                <thead>
                <tr>
                    <th>Heure</th>
                    <th>Lundi</th>
                    <th>Mardi</th>
                    <th>Mercredi</th>
                    <th>Jeudi</th>
                    <th>Vendredi</th>
                    <th>Samedi</th>
                    <th>Dimanche</th>
                </tr>
                </thead>
                <tbody>';

		foreach ( $planning as $hour => $courses ) {
			$return .= "<tr><td style='display: block;width: 100px'>" . $hour . "</td>";
			foreach ( $courses as $i => $subject ) {
				if ( $subject === 'NULL' ) {
					$return .= "<td></td>";
				} else {
					$return .= "<td>" . $subject . "</td>";
				}
			}
			$return .= "</tr>";
		}

		$return .= '</tbody></table>';

		return $return;
	}
}

new Scolabs();